package org.rsoft.fce;

import java.io.IOException;
import java.io.InputStream;

public interface FrameReader {

	void open();
	
	void close();
	
	FrameInstant[] readFrames(final InputStream inStream) throws IOException;
	
	FrameInstant[] readFrames(final InputStream inStream, int countLimit) throws IOException;
}
