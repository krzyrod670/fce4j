package org.rsoft.fce.components;

import java.util.Optional;

import org.rsoft.fce.DefId;
import org.rsoft.fce.FrameInstant;
import org.rsoft.fce.FrameParser;
import org.rsoft.fce.ParsedFrame;

public class Am01FrameParser implements FrameParser {
	
	@Override
	public Optional<ParsedFrame> parseNextFrame(byte[] buffer, int pos, int bufferLength) {

		ParsedFrame frame = null;
		
		int dataLen = (buffer.length < bufferLength ? buffer.length: bufferLength) - pos;
		
		if (dataLen >= 27) {
			
			if (buffer[pos] == 0x19) {
				
				byte xorCrc = (byte) 112;				
				for (int i = 2; i < 27; i++) {
					xorCrc ^= buffer[pos + i];
				}
				
				if (buffer[pos + 1] == xorCrc) {
					frame = new FrameInstant();
					frame.buffer = new byte[27];
					System.arraycopy(buffer, pos, frame.buffer, 0, 27);
					frame.setFrameDef(DefId.of("com.rsoft.am01.frames.measurements"));
				}
			}			
		}
		
		return Optional.ofNullable(frame);
	}
}
