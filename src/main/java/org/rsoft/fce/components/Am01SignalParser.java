package org.rsoft.fce.components;

import org.rsoft.fce.DefId;
import org.rsoft.fce.FrameInstant;
import org.rsoft.fce.IntegerSignalInstant;
import org.rsoft.fce.SignalInstant;
import org.rsoft.fce.SignalParser;

public class Am01SignalParser implements SignalParser {

	@Override
	public SignalInstant[] parse(final FrameInstant frame) {

		if (!DefId.of("com.rsoft.am01.frames.measurements").equals(frame.getFrameDef())) {
			return new SignalInstant[0];
		}

		byte seqNum = frame.buffer[2];
		int acc_x = readInt(frame.buffer, 3);
		int acc_y = readInt(frame.buffer, 5);
		int acc_temp = readInt(frame.buffer, 7);
		int mag_x = readInt(frame.buffer, 9);
		int mag_y = readInt(frame.buffer, 11);
		int mag_z = readInt(frame.buffer, 13);

		IntegerSignalInstant acc_x_signal = new IntegerSignalInstant();
		acc_x_signal.setValue(acc_x);
		acc_x_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.acc_x"));
		acc_x_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant acc_y_signal = new IntegerSignalInstant();
		acc_y_signal.setValue(acc_y);
		acc_y_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.acc_y"));
		acc_y_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant acc_temp_signal = new IntegerSignalInstant();
		acc_temp_signal.setValue(acc_temp);
		acc_temp_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.acc_temp"));
		acc_temp_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant mag_x_signal = new IntegerSignalInstant();
		mag_x_signal.setValue(mag_x);
		mag_x_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.mag_x"));
		mag_x_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant mag_y_signal = new IntegerSignalInstant();
		mag_y_signal.setValue(mag_y);
		mag_y_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.mag_y"));
		mag_y_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant mag_z_signal = new IntegerSignalInstant();
		mag_z_signal.setValue(mag_z);
		mag_z_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.mag_z"));
		mag_z_signal.setTimestamp(frame.getTimestamp());

		SignalInstant[] signals = new SignalInstant[] { acc_x_signal, acc_y_signal, acc_temp_signal, mag_x_signal,
				mag_y_signal, mag_z_signal };

		return signals;
	}

	int readInt(byte[] buffer, int offset) {
		int value = buffer[offset + 1] & 0xFF;
		value <<= 8;
		value |= ((int)buffer[offset]) & 0xFF;
		return value;		
	}
	

	int readInt2(byte[] buffer, int offset) {
		int value = buffer[offset] & 0xFF;
		value <<= 8;
		value |= ((int)buffer[offset + 1]) & 0xFF;
		return value;		
	}
}
