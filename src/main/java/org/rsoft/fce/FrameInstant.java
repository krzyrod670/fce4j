package org.rsoft.fce;

import lombok.Data;

@Data
public class FrameInstant extends ParsedFrame {
		
	Long timestamp;
}
