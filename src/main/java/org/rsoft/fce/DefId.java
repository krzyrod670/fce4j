package org.rsoft.fce;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import lombok.Data;
import lombok.NonNull;

@Data
public class DefId {

	@NonNull
	private final UUID guid;
	
	public static DefId of(String value) {
		 
		UUID guid = UUID.nameUUIDFromBytes(value.getBytes(StandardCharsets.UTF_8));
		
		return new DefId(guid);
	}
	
}
