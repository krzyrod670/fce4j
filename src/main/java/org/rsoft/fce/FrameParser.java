package org.rsoft.fce;

import java.util.Optional;

public interface FrameParser {

	Optional<ParsedFrame> parseNextFrame(byte[] buffer, int pos, int length);

}