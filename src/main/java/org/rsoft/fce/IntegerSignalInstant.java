package org.rsoft.fce;

import lombok.Data;

@Data
public class IntegerSignalInstant extends SignalInstant {
	
	private int value;
}
