package org.rsoft.fce;

import java.util.List;

import lombok.Data;

@Data
public class SignalStride {
		
	private DefId signalDef;
	
	private List<SignalInstant> instants;	
}
