package org.rsoft.fce;

import lombok.Data;

@Data
public class ParsedFrame {

	public byte[] buffer;

	public DefId frameDef;

}
