package org.rsoft.fce;

import lombok.Data;

@Data
public class SignalInstant {
	
	private Long timestamp;	
	
	private DefId signalDef;
	
}
