package org.rsoft.fce;

import java.util.Collection;

public interface SignalCollector {

	void add(Collection<SignalStride> signals);
	
}
