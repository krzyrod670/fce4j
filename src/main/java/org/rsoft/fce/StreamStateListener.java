package org.rsoft.fce;

public interface StreamStateListener {

	void corrupted();
	
	void opened();
	
	void closed();
}
