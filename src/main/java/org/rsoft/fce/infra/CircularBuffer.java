package org.rsoft.fce.infra;

import java.io.IOException;
import java.io.InputStream;

public class CircularBuffer implements GenericBuffer {

	private byte[] buf;

	private int f = -1; // start of data; -1 = empty
	private int s = 0; // next free index to store

	private static final byte[] empty = new byte[0];

	public CircularBuffer(int capacity) {
		if (capacity <= 0) {
			throw new IllegalArgumentException("capacity");
		}

		this.buf = new byte[capacity];
	}

	public boolean isEmpty() {
		return this.f < 0;
	}

	public int getSize() {

		if (f < 0)
			return 0;

		return (this.f >= this.s) ? this.s - this.f + this.buf.length : this.s - this.f;
	}

	public byte getAt(int index) {

		if (index < 0) {
			throw new IndexOutOfBoundsException("Invalid index: " + index);
		}

		if (f < 0) {
			throw new IllegalStateException("No Data");
		}

		int size = (this.f >= this.s) ? this.s - this.f + this.buf.length : this.s - this.f;

		if (index >= size) {
			throw new IndexOutOfBoundsException("Invalid index: " + index);
		}

		return this.buf[(this.f + index) % this.buf.length];
	}

	public byte[] getData() {

		if (f < 0) {
			return empty;
		}

		byte[] bufCopy;

		if (this.f < this.s) {
			bufCopy = new byte[this.s - this.f];
			System.arraycopy(this.buf, f, bufCopy, 0, bufCopy.length);
		} else {
			bufCopy = new byte[this.buf.length - this.f + this.s];
			int size1 = this.buf.length - this.f;
			System.arraycopy(this.buf, f, bufCopy, 0, size1);
			System.arraycopy(this.buf, 0, bufCopy, size1, bufCopy.length - size1);
		}

		return bufCopy;
	}

	public int getData(byte[] bufCopy) {

		if (f < 0) {
			return 0;
		}

		int numCopied = bufCopy.length;

		if (this.f < this.s) {

			int numAvailable = this.s - this.f;
			numCopied = numCopied <= numAvailable ? numCopied : numAvailable;

			System.arraycopy(this.buf, f, bufCopy, 0, numCopied);

		} else {

			int numAvailable = this.buf.length - this.f + this.s;
			numCopied = numCopied <= numAvailable ? numCopied : numAvailable;

			int numLinear = this.buf.length - this.f;

			if (numLinear >= numCopied) {
				System.arraycopy(this.buf, f, bufCopy, 0, numCopied);

			} else {
				System.arraycopy(this.buf, f, bufCopy, 0, numLinear);
				System.arraycopy(this.buf, 0, bufCopy, numLinear, numCopied - numLinear);
			}
		}

		return numCopied;
	}

	public int insert(byte[] bytes, int pos, int numBytesToRead) {

		if (null == bytes) {
			throw new IllegalArgumentException("bytes");
		}

		if (bytes.length == 0) {
			return 0;
		}

		if (pos < 0 || pos > bytes.length - 1) {
			throw new IllegalArgumentException("pos");
		}

		int numInserted = 0; // total bytes copied
		int bytesToRead = bytes.length - pos; // bytes available to copy from buffer passed in
		bytesToRead = bytesToRead < numBytesToRead ? bytesToRead : numBytesToRead;

		if (f < 0) {

			// buffer is empty
			numInserted = buf.length <= bytesToRead ? buf.length : bytesToRead;

			System.arraycopy(bytes, pos, buf, 0, numInserted);

			this.s = numInserted % buf.length;

			// mark start of data
			this.f = 0;

		} else if (f == s) {
			// buf is full
			// read just by 1 byte

			numInserted = 1;

			System.arraycopy(bytes, pos, buf, s, numInserted);

			this.s = (this.s + 1) % buf.length;

			// increment begin of data position; a byte is lost
			this.f = this.s;

		} else if (f > s) {

			// buf is not full nor empty ; some data is present before end of buf array

			numInserted = f - s;
			numInserted = numInserted <= bytesToRead ? numInserted : bytesToRead;

			System.arraycopy(bytes, pos, buf, s, numInserted);

			this.s += numInserted;
		} else if (f == 0) {
			// buf is not full nor empty ; data begins (f) before next free space (s) in buf
			// array - a single copy case

			int availableToRead = buf.length - s;

			numInserted = availableToRead <= bytesToRead ? availableToRead : bytesToRead;

			System.arraycopy(bytes, pos, buf, s, numInserted);

			this.s = (this.s + numInserted) % buf.length;

		} else if (f < s) {

			// buf is not full nor empty ; data begins (f) before next free space (s) in buf
			// array - a twice copy case

			int availableLinear = buf.length - s;

			if (bytesToRead <= availableLinear) {

				// copying linearly till end of buf array without overlapping

				numInserted = bytesToRead;

				System.arraycopy(bytes, pos, buf, s, numInserted);

				this.s = (this.s + numInserted) % buf.length;
			} else {

				// first part - copy linearly till end of buf array
				System.arraycopy(bytes, pos, buf, s, availableLinear);

				// overlapping
				bytesToRead -= availableLinear;

				int numOverlapped = bytesToRead < this.f ? bytesToRead : f;

				System.arraycopy(bytes, pos + availableLinear, buf, 0, numOverlapped);

				// total number of copied bytes
				numInserted = availableLinear + numOverlapped;

				this.s = numOverlapped;
			}
		}

		return numInserted;
	}

	public int readStream(final InputStream inStream) throws IOException {

		if (null == inStream) {
			throw new IllegalArgumentException("inStream");
		}

		int numInserted = 0; // total bytes copied

		if (f < 0) {

			// buffer is empty

			numInserted = inStream.read(buf, 0, buf.length);
			if (numInserted < 0)
				numInserted = 0;

			this.s = numInserted % buf.length;

			// mark start of data
			this.f = 0;

		} else if (f == s) {
			// buf is full
			// read just by 1 byte

			numInserted = inStream.read(buf, s, 1);
			if (numInserted < 0)
				numInserted = 0;

			this.s = (this.s + 1) % buf.length;

			// increment begin of data position; a byte is lost
			this.f = this.s;

		} else if (f > s) {

			// buf is not full nor empty ; some data is present before end of buf array

			numInserted = inStream.read(buf, s, f - s);
			if (numInserted < 0)
				numInserted = 0;

			this.s += numInserted;

		} else if (f == 0) {
			// buf is not full nor empty ; data begins (f) before next free space (s) in buf
			// array - a single copy case

			numInserted = inStream.read(buf, s, buf.length - s);
			if (numInserted < 0)
				numInserted = 0;

			this.s = (this.s + numInserted) % buf.length;

		} else if (f < s) {

			// buf is not full nor empty ; data begins (f) before next free space (s) in buf
			// array - a twice copy case

			int availableLinear = buf.length - s;

			numInserted = inStream.read(buf, s, availableLinear);
			if (numInserted < 0)
				numInserted = 0;

			if (numInserted == availableLinear) {
				// whole buffer is filled up to its linear length
				// read some more data from stream

				int numInserted2 = inStream.read(buf, 0, f);
				if (numInserted2 < 0)
					numInserted2 = 0;

				this.s = numInserted2;

				numInserted += numInserted2;

			} else {
				// buffer was not filled up to its linear length because input stream was
				// drained

				this.s = this.s + numInserted;
			}
		}

		return numInserted;
	}

	public int remove(int numBytes) {

		if (numBytes < 0) {
			throw new IllegalArgumentException("numBytes");
		}

		if (f < 0) {
			throw new IllegalStateException("No Data");
		}

		int numAvailable = s == f ? buf.length : (s - f + buf.length) % buf.length;

		int numRemoved = numAvailable < numBytes ? numAvailable : numBytes;

		this.f = (this.f + numRemoved) % buf.length;

		if (this.f == this.s) {
			this.f = -1; // buf is emptied
		}

		return numRemoved;
	}

}
