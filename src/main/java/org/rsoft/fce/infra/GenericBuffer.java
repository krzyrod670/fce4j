package org.rsoft.fce.infra;

public interface GenericBuffer {

	public boolean isEmpty();
	
	public int getSize();
	
	public byte getAt(int index);
	
	public byte[] getData();
	
	public int getData(byte[] buffer);
}
