package org.rsoft.fce;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.rsoft.fce.infra.CircularBuffer;

import lombok.Data;

@Data
public class BufferedFrameReader implements FrameReader {

	private CircularBuffer buffer;

	private FrameParser frameParser;

	private byte[] parseBuffer;

	@Override
	public FrameInstant[] readFrames(InputStream inStream) throws IOException {

		final List<FrameInstant> frames = new ArrayList<>();

		while (consume(inStream)) {

			int dataLength = this.buffer.getData(this.parseBuffer);

			int numParsedBytes = parseFrames(dataLength, frames);

			// reclaiming buffer space. processed bytes can be freed
			reclaim(numParsedBytes);
		}

		return frames.toArray(new FrameInstant[frames.size()]);
	}

	@Override
	public FrameInstant[] readFrames(final InputStream inStream, int countLimit) throws IOException	{
		
		final List<FrameInstant> frames = new ArrayList<>(countLimit);

		while (consume(inStream) && frames.size() < countLimit) {

			int dataLength = this.buffer.getData(this.parseBuffer);

			int numParsedBytes = parseFrames(dataLength, frames, countLimit - frames.size());

			// reclaiming buffer space. processed bytes can be freed
			reclaim(numParsedBytes);
		}

		return frames.toArray(new FrameInstant[frames.size()]);
		
	}
	
	public void open() {
		int maxFrameLength = getMaxFrameLength();
		this.buffer = new CircularBuffer(2 * maxFrameLength);
		this.parseBuffer = new byte[maxFrameLength * 2];
	}

	public void close() {
		this.buffer = null;
		this.parseBuffer = null;
	}

	private int parseFrames(int dataLength, final List<FrameInstant> frames) {

		int parsePos = 0;
		while (parsePos <= dataLength - getMinFrameLength()) {

			Optional<ParsedFrame> frame = this.frameParser.parseNextFrame(this.parseBuffer, parsePos, dataLength);

			if (frame.isPresent()) {

				final FrameInstant instant = new FrameInstant();
				instant.setBuffer(frame.get().getBuffer());
				instant.setFrameDef(frame.get().getFrameDef());
				instant.setTimestamp(new Date().getTime());

				frames.add(instant);

				parsePos += frame.get().getBuffer().length;

			} else {
				++parsePos;
			}
		}

		return parsePos;
	}

	private int parseFrames(int dataLength, final List<FrameInstant> frames, int countLimit) {

		int parsePos = 0;
		while (parsePos <= dataLength - getMinFrameLength() && countLimit > 0) {

			Optional<ParsedFrame> frame = this.frameParser.parseNextFrame(this.parseBuffer, parsePos, dataLength);

			if (frame.isPresent()) {

				final FrameInstant instant = new FrameInstant();
				instant.setBuffer(frame.get().getBuffer());
				instant.setFrameDef(frame.get().getFrameDef());
				instant.setTimestamp(new Date().getTime());

				frames.add(instant);
				--countLimit;
				
				parsePos += frame.get().getBuffer().length;

			} else {
				++parsePos;
			}
		}

		return parsePos;
	}
	
	private void reclaim(int numBytes) {
		// reclaim limit: in case buffer is tailed with the longest frame minus 1 final byte that will arrive later
		int reclaimLimit = this.buffer.getSize() - getMaxFrameLength() + 1;
		int numReclaimed = numBytes < reclaimLimit ? numBytes : reclaimLimit;
		if (numReclaimed > 0) {
			this.buffer.remove(numReclaimed);	
		}
		
	}

	private boolean consume(final InputStream inStream) throws IOException {
		return this.buffer.readStream(inStream) > 0;
	}

	private int getMinFrameLength() {
		return 27; // TODO read from stream definition
	}

	private int getMaxFrameLength() {
		return 27; // TODO read from stream definition
	}

}
