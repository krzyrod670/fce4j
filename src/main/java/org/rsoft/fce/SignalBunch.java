package org.rsoft.fce;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Data;
import lombok.NonNull;

@Data
public class SignalBunch {

	private List<SignalStride> signals = new ArrayList<>();
		
	public void add(final @NonNull SignalStride[] strides) {		
		for (final SignalStride str : strides) {
			
			Optional<SignalStride> signal = this.signals.stream()
				.filter(sig -> sig.getSignalDef().equals(str.getSignalDef()))
				.findFirst();
			
			if (signal.isPresent()) {
				signal.get().getInstants().addAll(0, str.getInstants());
			}
			else {
				this.signals.add(str);
			}					
		}				
	}
	
	public SignalStride getSignal(int index) {
		return signals.get(index);
	}
	
}
