package org.rsoft.fce;

public interface FrameCollector {

	void add(FrameInstant[] frames);
}
