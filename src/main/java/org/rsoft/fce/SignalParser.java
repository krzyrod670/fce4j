package org.rsoft.fce;

public interface SignalParser {

	SignalInstant[] parse(FrameInstant frame);
	
}
