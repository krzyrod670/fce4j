package org.rsoft.fce;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lombok.Data;

@Data
public class StreamReader {

	private StreamContext context;

	private SignalCollector signalCollector;

	private FrameCollector frameCollector;

	private StreamStateListener stateListener;

	private FrameReader frameReader;

	private SignalParser signalParser;

	public void open() {
		frameReader.open();
	}

	public void close() throws IOException {
		frameReader.close();
	}

	public SignalStride[] read(final InputStream inStream) throws IOException {

		final FrameInstant[] frames = frameReader.readFrames(inStream);

		if (frameCollector != null) {
			frameCollector.add(frames);
		}

		final SignalStride[] signals = extractSignals(frames);
		
		return signals;
	}


	public SignalStride[] read(final InputStream inStream, int countLimit) throws IOException {

		final FrameInstant[] frames = frameReader.readFrames(inStream, countLimit);

		if (frameCollector != null) {
			frameCollector.add(frames);
		}

		final SignalStride[] signals = extractSignals(frames);
		
		return signals;
	}
	

	private SignalStride[] extractSignals(final FrameInstant[] frames) {
		
		List<SignalStride> strides = new ArrayList<>();
		for (final FrameInstant f : frames) {
			SignalInstant[] signals = signalParser.parse(f);

			for (final SignalInstant s : signals) {

				Optional<SignalStride> stride = strides.stream().filter(str -> s.getSignalDef().equals(str.getSignalDef())).findFirst();
				
				if (stride.isPresent()) {
					stride.get().getInstants().add(s);
				}
				else {
					SignalStride newStride = new SignalStride();
					newStride.setSignalDef(s.getSignalDef());
					newStride.setInstants(new ArrayList<>(Arrays.asList(s)));
					strides.add(newStride);
				}				
			}

		}

		return strides.toArray(new SignalStride[strides.size()]);
	}
	
}
