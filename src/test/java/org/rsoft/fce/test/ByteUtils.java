package org.rsoft.fce.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ByteUtils {

	public static byte[][] crackBytes(byte[] bytes, int[] chunksPattern) {
		
		// calcuate count of bufs		
		int count = 0;
		
		int total = 0;
		int numOfPattern = 0;
		while (total < bytes.length) {
			total += chunksPattern[numOfPattern];
			count ++;			
			numOfPattern = (numOfPattern + 1) % chunksPattern.length;
		}
		
		byte[][] bufs = new byte[count][];
		int bytesPos = 0;
		int bufs_length = 0;
		for (int i = 0; i < count; i++) {
			
			int len = chunksPattern[i % chunksPattern.length];						
			if (bytesPos + len > bytes.length) {
				len = bytes.length - bytesPos; 
			}
			
			bufs[i] = new byte[len];
			System.arraycopy(bytes, bytesPos, bufs[i], 0, len);			
			bytesPos += len;			
			bufs_length += len;
		}
		
		assertEquals(bytes.length, bufs_length);
		
		return bufs;
	}
	
	public static byte[] readHexFile(String resource) {

		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		final File file = new File(classLoader.getResource(resource).getFile());

		// Read File Content
		String content;
		try {
			content = new String(Files.readAllBytes(file.toPath()));
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		final String[] bytesAsHex = content.trim().split("\\s+");
		final byte[] bytes = new byte[bytesAsHex.length];
		for (int i = 0; i < bytesAsHex.length; i++) {
			if (bytesAsHex[i].length() != 2)
				throw new IllegalArgumentException("Resource contains a hex which length is not 2 but "
						+ bytesAsHex[i].length() + " at hex num " + i + " value: " + bytesAsHex[i]);
			bytes[i] = (byte) Integer.parseInt(bytesAsHex[i], 16);
		}

		return bytes;
	}

}
