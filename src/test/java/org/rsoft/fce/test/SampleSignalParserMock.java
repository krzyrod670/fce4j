package org.rsoft.fce.test;

import org.rsoft.fce.DefId;
import org.rsoft.fce.FrameInstant;
import org.rsoft.fce.IntegerSignalInstant;
import org.rsoft.fce.SignalInstant;
import org.rsoft.fce.SignalParser;

public class SampleSignalParserMock implements SignalParser {

	@Override
	public SignalInstant[] parse(final FrameInstant frame) {

		if (!DefId.of("com.rsoft.am01.frames.measurements").equals(frame.getFrameDef())) {
			return new SignalInstant[0];
		}

		int acc_x = frame.buffer[2 + 0] + (int) frame.buffer[2 + 1] * 256;
		int acc_y = frame.buffer[2 + 2] + (int) frame.buffer[2 + 3] * 256;
		int acc_temp = frame.buffer[2 + 4] + (int) frame.buffer[2 + 5] * 256;
		int mag_x = frame.buffer[2 + 6] + (int) frame.buffer[2 + 7] * 256;
		int mag_y = frame.buffer[2 + 8] + (int) frame.buffer[2 + 9] * 256;
		int mag_z = frame.buffer[2 + 10] + (int) frame.buffer[2 + 11] * 256;

		IntegerSignalInstant acc_x_signal = new IntegerSignalInstant();
		acc_x_signal.setValue(acc_x);
		acc_x_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.acc_x"));
		acc_x_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant acc_y_signal = new IntegerSignalInstant();
		acc_y_signal.setValue(acc_y);
		acc_y_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.acc_y"));
		acc_y_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant acc_temp_signal = new IntegerSignalInstant();
		acc_temp_signal.setValue(acc_temp);
		acc_temp_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.acc_temp"));
		acc_temp_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant mag_x_signal = new IntegerSignalInstant();
		mag_x_signal.setValue(mag_x);
		mag_x_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.mag_x"));
		mag_x_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant mag_y_signal = new IntegerSignalInstant();
		mag_y_signal.setValue(mag_y);
		mag_y_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.mag_y"));
		mag_y_signal.setTimestamp(frame.getTimestamp());

		IntegerSignalInstant mag_z_signal = new IntegerSignalInstant();
		mag_z_signal.setValue(mag_z);
		mag_z_signal.setSignalDef(DefId.of("com.rsoft.am01.signals.mag_z"));
		mag_z_signal.setTimestamp(frame.getTimestamp());

		SignalInstant[] signals = new SignalInstant[] { acc_x_signal, acc_y_signal, acc_temp_signal, mag_x_signal,
				mag_y_signal, mag_z_signal };

		return signals;
	}

}
