package org.rsoft.fce.test;

import org.rsoft.fce.BufferedFrameReader;
import org.rsoft.fce.FrameParser;
import org.rsoft.fce.FrameReader;
import org.rsoft.fce.SignalParser;

public abstract class AbstractFceTest {

	protected final static byte[] sample1 =  ByteUtils.readHexFile("sample1.txt");
	protected final static byte[] sample2 =  ByteUtils.readHexFile("sample2.txt");
	protected final static byte[] sample3 =  ByteUtils.readHexFile("sample3.txt");
	
	protected final static FrameParser sampleFrameParser = new SampleFrameParserMock();
	protected static final SignalParser sampleSignalParser = new SampleSignalParserMock();	
	protected static final FrameReader sampleFrameReader;
	static {
		final BufferedFrameReader bufferedFrameReader = new BufferedFrameReader();
		bufferedFrameReader.setFrameParser(sampleFrameParser);
		sampleFrameReader = bufferedFrameReader; 
	}
	
}
