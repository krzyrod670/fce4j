package org.rsoft.fce;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.rsoft.fce.FrameCollector;
import org.rsoft.fce.FrameInstant;
import org.rsoft.fce.SignalBunch;
import org.rsoft.fce.SignalStride;
import org.rsoft.fce.StreamReader;
import org.rsoft.fce.test.AbstractFceTest;
import org.rsoft.fce.test.ByteUtils;
import org.rsoft.fce.test.CrackPatterns;

class StreamReaderTest extends AbstractFceTest {
	
	@Test
	void smokeTest() throws IOException {
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_100).size());
		assertEquals(165, readFramesFromChunkedInput(sample2, CrackPatterns.pattern1_100).size());
	}	
	
	@Test
	void singlePacketSignalsTest() throws IOException {

		StreamReader streamReader = new StreamReader();
		streamReader.setFrameReader(sampleFrameReader);
		streamReader.setSignalParser(sampleSignalParser);

		streamReader.open();

		// read single block from input stream
		final ByteArrayInputStream inStream = new ByteArrayInputStream(sample1);

		final SignalStride[] strides = streamReader.read(inStream);

		assertEquals(6, strides.length);
		assertEquals(165, strides[0].getInstants().size());
		assertEquals(165, strides[1].getInstants().size());
		assertEquals(165, strides[2].getInstants().size());
		assertEquals(165, strides[3].getInstants().size());
		assertEquals(165, strides[4].getInstants().size());
		assertEquals(165, strides[5].getInstants().size());		
	}


	/** content is passed to stream processor as single continuous block */
	@Test
	void singlePacketFramesTest() throws IOException {

		final List<FrameInstant> readFrames = new ArrayList<>();

		StreamReader streamReader = new StreamReader();
		streamReader.setFrameReader(sampleFrameReader);
		streamReader.setSignalParser(sampleSignalParser);
		streamReader.setFrameCollector(new FrameCollector() {

			@Override
			public void add(FrameInstant[] frames) {
				readFrames.addAll(Arrays.asList(frames));
			}

		});

		streamReader.open();

		// read single block from input stream
		final ByteArrayInputStream inStream = new ByteArrayInputStream(sample1);
		streamReader.read(inStream);

		assertEquals(165, readFrames.size());
	}

	/** content is passed to stream processor as single continuous block */
	@Test
	void readWithCountLimit() throws IOException {
		
		int countLimit = 10;
		
		final List<FrameInstant> readFrames = new ArrayList<>();

		StreamReader streamReader = new StreamReader();
		streamReader.setFrameReader(sampleFrameReader);
		streamReader.setSignalParser(sampleSignalParser);
		streamReader.setFrameCollector(new FrameCollector() {

			@Override
			public void add(FrameInstant[] frames) {
				readFrames.addAll(Arrays.asList(frames));
			}

		});

		streamReader.open();

		
		// read single block from input stream
		final ByteArrayInputStream inStream = new ByteArrayInputStream(sample1);
		while (streamReader.read(inStream, countLimit).length > 0) 
			;

		assertEquals(165, readFrames.size());
	}

	
	

	/** Content is passed as a set of chunks of variable (pseudo-random) lengths*/
	@Test
	void chunkedPacketsSignalsTest() throws IOException {
		
		// expecting 165 signal instants in each stride, 6 signals
		SignalBunch signalBunch = readSignalsFromChunkedInput(sample1, CrackPatterns.pattern1_2);
		assertEquals(6, signalBunch.getSignals().size());
		assertEquals(165, signalBunch.getSignal(0).getInstants().size());
		assertEquals(165, signalBunch.getSignal(1).getInstants().size());
		assertEquals(165, signalBunch.getSignal(2).getInstants().size());
		assertEquals(165, signalBunch.getSignal(3).getInstants().size());
		assertEquals(165, signalBunch.getSignal(4).getInstants().size());
		assertEquals(165, signalBunch.getSignal(5).getInstants().size());

		signalBunch = readSignalsFromChunkedInput(sample1, CrackPatterns.pattern1_3);
		assertEquals(6, signalBunch.getSignals().size());
		assertEquals(165, signalBunch.getSignal(0).getInstants().size());
		assertEquals(165, signalBunch.getSignal(1).getInstants().size());
		assertEquals(165, signalBunch.getSignal(2).getInstants().size());
		assertEquals(165, signalBunch.getSignal(3).getInstants().size());
		assertEquals(165, signalBunch.getSignal(4).getInstants().size());
		assertEquals(165, signalBunch.getSignal(5).getInstants().size());
	
		signalBunch = readSignalsFromChunkedInput(sample1, CrackPatterns.pattern1_5);
		assertEquals(6, signalBunch.getSignals().size());
		assertEquals(165, signalBunch.getSignal(0).getInstants().size());
		assertEquals(165, signalBunch.getSignal(1).getInstants().size());
		assertEquals(165, signalBunch.getSignal(2).getInstants().size());
		assertEquals(165, signalBunch.getSignal(3).getInstants().size());
		assertEquals(165, signalBunch.getSignal(4).getInstants().size());
		assertEquals(165, signalBunch.getSignal(5).getInstants().size());
		
		signalBunch = readSignalsFromChunkedInput(sample1, CrackPatterns.pattern1_100);		
		assertEquals(6, signalBunch.getSignals().size());
		assertEquals(165, signalBunch.getSignal(0).getInstants().size());
		assertEquals(165, signalBunch.getSignal(1).getInstants().size());
		assertEquals(165, signalBunch.getSignal(2).getInstants().size());
		assertEquals(165, signalBunch.getSignal(3).getInstants().size());
		assertEquals(165, signalBunch.getSignal(4).getInstants().size());
		assertEquals(165, signalBunch.getSignal(5).getInstants().size());

	}


	
	
	/** Content is passed in chunked form of constant lengths */ 
	@Test
	void constantSizePacketsSignalsTest() throws IOException {
		
		/** crack sample1 into packets of constant lengths 1.. {size of sample1} */
		for (int packetSize = 1; packetSize <= sample1.length; packetSize++) {

			SignalBunch signalBunch = readSignalsFromChunkedInput(sample1, new int[] { packetSize });
			assertEquals(6, signalBunch.getSignals().size());
			assertEquals(165, signalBunch.getSignal(0).getInstants().size());
			assertEquals(165, signalBunch.getSignal(1).getInstants().size());
			assertEquals(165, signalBunch.getSignal(2).getInstants().size());
			assertEquals(165, signalBunch.getSignal(3).getInstants().size());
			assertEquals(165, signalBunch.getSignal(4).getInstants().size());
			assertEquals(165, signalBunch.getSignal(5).getInstants().size());
		}
	}
	

	/** Content is passed as a set of chunks of variable (pseudo-random) lengths*/
	@Test
	void chunkedPacketsFramesTest() throws IOException {

		// expecting 165 frames
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_2).size());
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_3).size());
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_5).size());
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_100).size());
	}


	/** Content is passed in chunked form of constant lengths */ 
	@Test
	void constantSizePacketsFramesTest() throws IOException {
		
		/** crack sample1 into packets of constant lengths 1.. {size of sample1} */
		for (int packetSize = 1; packetSize <= sample1.length; packetSize++) {
			assertEquals(165, readFramesFromChunkedInput(sample1, new int[] { packetSize }).size());
		}
	}
		
	private final List<FrameInstant> readFramesFromChunkedInput(byte[] input, int[] chunkSizePattern) throws IOException {

		final List<FrameInstant> collectedFrames = new ArrayList<>();

		final StreamReader streamReader = new StreamReader();
		streamReader.setFrameReader(sampleFrameReader);
		streamReader.setSignalParser(sampleSignalParser);
		streamReader.setFrameCollector(new FrameCollector() {

			@Override
			public void add(FrameInstant[] frames) {
				collectedFrames.addAll(Arrays.asList(frames));
			}

		});

		streamReader.open();

		// content is passed to stream reader as a set of chunks in multiple read() invocations 
		byte[][] packets = ByteUtils.crackBytes(sample1, CrackPatterns.pattern1_100);
		for (final byte[] p : packets) {

			final ByteArrayInputStream inStream = new ByteArrayInputStream(p);
			streamReader.read(inStream);
		}

		return collectedFrames;
	}
		
	private final SignalBunch readSignalsFromChunkedInput(byte[] input, int[] chunkSizePattern) throws IOException {

		
		SignalBunch signals = new SignalBunch();
		
		final StreamReader streamReader = new StreamReader();
		streamReader.setFrameReader(sampleFrameReader);
		streamReader.setSignalParser(sampleSignalParser);
		streamReader.open();

		// content is passed to stream reader as a set of chunks in multiple read() invocations 
		byte[][] packets = ByteUtils.crackBytes(sample1, CrackPatterns.pattern1_100);
		for (final byte[] p : packets) {
			final ByteArrayInputStream inStream = new ByteArrayInputStream(p);
			final SignalStride[] strides = streamReader.read(inStream);
			signals.add(strides);
		}

		return signals;
	}	
}
