package org.rsoft.fce.infra;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.rsoft.fce.test.ByteUtils;
import org.rsoft.fce.test.CrackPatterns;

class CircularBufferTest {
	
	@Test
	public void smokeTest() {		
		testBufferOperationByArrayInput(200, CrackPatterns.pattern1_100 , 100, ByteUtils.readHexFile("sample1.txt"));
		testBufferOperationByArrayInput(200, CrackPatterns.pattern1_100 , 100, ByteUtils.readHexFile("sample2.txt"));
		testBufferOperationByArrayInput(200, CrackPatterns.pattern1_100 , 100, ByteUtils.readHexFile("sample3.txt"));
	}
	
	@Test
	void insertWithOverflowTest() {

		CircularBuffer buf = new CircularBuffer(100);

		assertTrue(buf.isEmpty());
		assertEquals(0, buf.getSize());
		assertThrows(IllegalStateException.class, () -> buf.getAt(0));

		byte[] data = buf.getData();
		assertEquals(0, data.length);

		List<Byte> valuesStoredInBuffer = new ArrayList<>();

		for (int i = 0; i < 100; i++) {

			byte b = (byte) (101 + i);

			valuesStoredInBuffer.add(b);

			buf.insert(new byte[] { b }, 0, 1);

			assertFalse(buf.isEmpty());

			assertEquals(i + 1, buf.getSize());

			for (int j = 0; j <= i; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), buf.getAt(j));
			}

			final int aboveIndex = i + 1;
			assertThrows(IndexOutOfBoundsException.class, () -> buf.getAt(aboveIndex));
			assertThrows(IndexOutOfBoundsException.class, () -> buf.getAt(-1));

			data = buf.getData();
			assertEquals(i + 1, data.length);
			for (int j = 0; j < data.length; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), data[j]);
			}
		}

		// buf is now filled

		assertFalse(buf.isEmpty());
		assertEquals(100, buf.getSize());

		data = buf.getData();
		assertEquals(100, data.length);

		// insert overflowing bytes

		for (int i = 0; i < 1000; i++) {

			byte b = (byte) (201 + i);

			valuesStoredInBuffer.remove(0);
			valuesStoredInBuffer.add(b);

			buf.insert(new byte[] { b }, 0, 1);

			assertFalse(buf.isEmpty());

			assertEquals(100, buf.getSize());

			for (int j = 0; j < 100; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), buf.getAt(j));
			}

			data = buf.getData();
			assertEquals(100, data.length);
			for (int j = 0; j < 100; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), data[j]);
			}
		}
	}

	
	@Test
	void getDataToPreAllocBufferTest() {

		CircularBuffer buf = new CircularBuffer(100);

		byte[] preAllocBuffer = new byte[20];
		
		List<Byte> valuesStoredInBuffer = new ArrayList<>();

		for (int i = 0; i < 20; i++) {

			byte b = (byte) (101 + i);

			valuesStoredInBuffer.add(b);

			buf.insert(new byte[] { b }, 0, 1);

			int num = buf.getData(preAllocBuffer);
			assertEquals(i + 1, num);
			for (int j = 0; j < num; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), preAllocBuffer[j]);
			}
		}

		
		for (int i = 20; i < 100; i++) {

			byte b = (byte) (101 + i);

			valuesStoredInBuffer.add(b);

			buf.insert(new byte[] { b }, 0, 1);

			int num = buf.getData(preAllocBuffer);
			assertEquals(20, num);
			for (int j = 0; j < 20 ; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), preAllocBuffer[j]);
			}
		}

		// buf is now filled

		// insert overflowing bytes

		for (int i = 0; i < 1000; i++) {

			byte b = (byte) (201 + i);

			valuesStoredInBuffer.remove(0);
			valuesStoredInBuffer.add(b);

			buf.insert(new byte[] { b }, 0, 1);

			int num = buf.getData(preAllocBuffer);
			assertEquals(20, num);
			for (int j = 0; j < 20 ; j++) {
				assertEquals((byte) valuesStoredInBuffer.get(j), preAllocBuffer[j]);
			}
		}
	}

	@Test
	public void packetSizeSweepTest() {
		
		byte[] incomingContent = ByteUtils.readHexFile("sample1.txt");

		for (int bufferSize = 100; bufferSize < 1024; bufferSize++) {
			for (int packetSize = 1; packetSize <= 50; packetSize++) {		
				testBufferOperationByArrayInput(bufferSize, new int[] { packetSize } , packetSize, incomingContent);
			}
		}				
	}

	@Test
	public void randomPacketSizeTest() {
		
		byte[] incomingContent = ByteUtils.readHexFile("sample1.txt");

		for (int bufferSize = 100; bufferSize <= 1024; bufferSize++) {
			for (int packetSize = 1; packetSize <= 50; packetSize++) {
		
				// convert crack pattern values to match range of [1.. packetSize]
				final int ps = packetSize;
				int[] packetPattern = Arrays.stream(CrackPatterns.pattern1_100).map(i -> (i % ps) + 1).toArray();
				
				testBufferOperationByArrayInput(bufferSize, packetPattern , packetSize, incomingContent);
			}
		}				
	}	
	
	@Test
	public void singlePacketStreamTest() throws IOException {
		
		byte[] incomingContent = ByteUtils.readHexFile("sample1.txt");

		int bufferSize = 100;
		
		testBufferOperationByInputStream(bufferSize, new int[] {0} , 50, incomingContent);
	}	

	
	
	// writes to buffer a series of packets and reads by a fixed amount of bytes, then compares input and output
	private void testBufferOperationByArrayInput(int sizeOfBuffer, int[] incomingPacketPattern, int sizeOfRead, byte[] incomingContent) {
		
		final CircularBuffer buf = new CircularBuffer(sizeOfBuffer);
		byte[] readContent = new byte[incomingContent.length];
		byte[][] packets = ByteUtils.crackBytes(incomingContent, incomingPacketPattern);
		
		int numReceived = 0;
		int numPacket = 0;
		int packetPos = 0;		
		
		int totalInserted = 0;
		
		while (numReceived < incomingContent.length) {
		
			// write buf	
			if (numPacket < packets.length) {
				int nnn = buf.insert(packets[numPacket], packetPos, packets[numPacket].length - packetPos);				
				packetPos += nnn;
				totalInserted += nnn;				
				assertTrue(packetPos <= packets[numPacket].length);
				
				if (packetPos == packets[numPacket].length) {
					++numPacket;
					packetPos = 0;				
				}
			}
			
			// read buf
			
			if (buf.getSize() >= sizeOfRead) {
				byte[] data = buf.getData();
				System.arraycopy(data, 0, readContent, numReceived, sizeOfRead);
				numReceived += sizeOfRead;
				buf.remove(sizeOfRead);			
			}
			else if (numPacket == packets.length) {
				// remaining
				byte[] data = buf.getData();
				
				assertEquals(buf.getSize(), data.length);
				System.arraycopy(data, 0, readContent, numReceived, buf.getSize());
				numReceived += buf.getSize();
				buf.remove(buf.getSize());
			}
			
			
		}	
		
		assertEquals(incomingContent.length, totalInserted);
		assertEquals(incomingContent.length, numReceived);
		assertArrayEquals(incomingContent, readContent);				
	}
	
	
	private void testBufferOperationByInputStream(int sizeOfBuffer, int[] incomingPacketPattern, int sizeOfRead, byte[] incomingContent) throws IOException {
		
		ByteArrayInputStream inStream = new ByteArrayInputStream(incomingContent);
		
		final CircularBuffer buf = new CircularBuffer(sizeOfBuffer);
		byte[] readContent = new byte[incomingContent.length];
				
		int numReceived = 0;		
		
		while (numReceived < incomingContent.length) {
			
			buf.readStream(inStream);
						
			// read buf
			
			byte[] data = buf.getData();
			
			int numRead = sizeOfRead <= data.length ? sizeOfRead : data.length;
			System.arraycopy(data, 0, readContent, numReceived, numRead);
			numReceived += numRead;			
			buf.remove(numRead);
		}	
		
		assertEquals(incomingContent.length, numReceived);
		assertArrayEquals(incomingContent, readContent);				
	}
}
