package org.rsoft.fce;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.rsoft.fce.BufferedFrameReader;
import org.rsoft.fce.FrameInstant;
import org.rsoft.fce.test.AbstractFceTest;
import org.rsoft.fce.test.ByteUtils;
import org.rsoft.fce.test.CrackPatterns;

class BufferedFrameReaderTest extends AbstractFceTest {
	
	@Test
	void smokeTest() throws IOException {
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_100).length);
		assertEquals(165, readFramesFromChunkedInput(sample2, CrackPatterns.pattern1_100).length);
	}
	
	/** content is passed to stream processor as single continuous block */
	@Test
	void singleInputBlockTest() throws IOException {

		final BufferedFrameReader reader = new BufferedFrameReader();
		reader.setFrameParser(sampleFrameParser);
		reader.open();

		// read single block from input stream
		final ByteArrayInputStream inStream = new ByteArrayInputStream(sample1);
		FrameInstant[] readFrames = reader.readFrames(inStream);

		assertEquals(165, readFrames.length);
	}

	/** Content is passed as a set of chunks of variable (pseudo-random) lengths*/
	@Test
	void chunkedInputBlockTest() throws IOException {

		// expecting 165 frames
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_2).length);
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_3).length);
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_5).length);
		assertEquals(165, readFramesFromChunkedInput(sample1, CrackPatterns.pattern1_100).length);
	}

	/** Content is passed in chunked form of constant lengths */ 
	@Test
	void constantSizeChunksTest() throws IOException {
		
		/** crack sample1 into packets of constant lengths 1.. {size of sample1} */
		for (int packetSize = 1; packetSize <= sample1.length; packetSize++) {
			assertEquals(165, readFramesFromChunkedInput(sample1, new int[] { packetSize }).length);
		}
	}
	
	private final FrameInstant[] readFramesFromChunkedInput(byte[] input, int[] chunkSizePattern) throws IOException {
		
		final BufferedFrameReader streamProcessor = new BufferedFrameReader();
		streamProcessor.setFrameParser(sampleFrameParser);
		streamProcessor.open();

		// content is passed to stream reader as a set of chunks in multiple read() invocations 
		byte[][] inChunks = ByteUtils.crackBytes(sample1, CrackPatterns.pattern1_100);
		final List<FrameInstant[]> frames = new ArrayList<>(); 
		for (final byte[] p : inChunks) {

			final ByteArrayInputStream inStream = new ByteArrayInputStream(p);
			frames.add(streamProcessor.readFrames(inStream));			
		}

		return frames.stream().flatMap(Arrays::stream).toArray(FrameInstant[]::new);
	}

}
